/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author FASILKOM
 */
public class Universitas {
    private String nm_university;
    private double total_students;
    private double growth_per_year;
    private double choosed_year;
    
    public Universitas(String univ, double studs, double growth, double years){
        setNmUniversity(univ);
        setTotalStudents(studs);
        setGrowthPerYear(growth);
        setChoosedYear(years);
    }
    
    void setNmUniversity(String univ){
        nm_university = univ;
    }
    void setTotalStudents(double studs){
        total_students = studs;
    }
    void setGrowthPerYear(double growth){
        growth_per_year = growth;
    }
    void setChoosedYear(double years){
        choosed_year = years;
    }
    
    public double getHitungJumlahMhs(){
        double hasil = total_students;
        for(int i=0; i<choosed_year;i++){
            hasil = hasil + ( (growth_per_year/100) * hasil);
        }
        return hasil;
    }
}
